<?php
/* 
 * Copyright (C) PowerOn Sistemas - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Lucas Sosa <sosalucas87@gmail.com>, Diciembre 2020
 */
declare(strict_types=1);

use DI\ContainerBuilder;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Validation;

use Hashids\Hashids;
return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        LoggerInterface::class => function (ContainerInterface $c) {
            $settings = $c->get('settings');

            $loggerSettings = $settings['logger'];
            $logger = new Logger($loggerSettings['name']);

            $processor = new UidProcessor();
            $logger->pushProcessor($processor);

            $handler = new StreamHandler($loggerSettings['path'], $loggerSettings['level']);
            $logger->pushHandler($handler);

            return $logger;
        },
        EntityManager::class => function (ContainerInterface $c) {
            $settings = $c->get('settings')['doctrine'];
            $config = Setup::createAnnotationMetadataConfiguration($settings['entitiesPaths'], $settings['debug']);

            return EntityManager::create($settings['dbParams'], $config);
        },
        ValidatorInterface::class => function (ContainerInterface $c) {
            $settings = $c->get('settings')['validation'];
            $validationBuilder = Validation::createValidatorBuilder();
            
            return $validationBuilder->addXmlMapping($settings['xmlMapping'])->getValidator();
        },
        Hashids::class => function (ContainerInterface $c) {
            $settings = $c->get('settings')['hashids'];
            
            return new Hashids($settings['salt'], 3);
        }
    ]);
};
