<?php
/* 
 * Copyright (C) PowerOn Sistemas - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Lucas Sosa <sosalucas87@gmail.com>, Diciembre 2020
 */
namespace App\Domain\Entities;
use Symfony\Component\Validator\Constraint as Assert;
/** 
 * @Entity(repositoryClass="App\Domain\Repositories\ProductRepository")
 * @Table(name="products")
 */
class Product {
    /** 
     * @Id
     * @Column(type="integer") 
     * @GeneratedValue
     */ 
    private $id;
    
    /** 
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 2, 
     *      max = 100,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     * )
     * @Column(length=100) 
     */
    private $brand;
    
    /** @Column(length=100) */
    private $model;
    /** @Column(length=7, nullable=true) */
    private $color;
    /** @Column(type="decimal", precision=10, scale=2, nullable=true) */
    private $weight;
    /** @Column(type="decimal", precision=10, scale=2, nullable=true) */
    private $salePrice;
    /** @Column(type="decimal", precision=10, scale=2, nullable=true) */
    private $costPrice;
    /** 
     * @Assert\NotBlank
     * @Column(length=3, nullable=true) 
     */
    private $costCurrency;
    /** @Column(length=3, nullable=true) */
    private $saleCurrency;
    /** @Column(length=100, nullable=true) */
    private $image;
    /** @Column(type="datetime") */
    private $createdDate;
    /** @Column(type="integer") */
    private $stock = 0;
    /** @Column(type="text", nullable=true) */
    private $description;
    /** @Column(type="array", nullable=true) */
    private $tags = [];
    
    function getId() {
        return $this->id;
    }

    function getBrand() {
        return $this->brand;
    }

    function getModel() {
        return $this->model;
    }

    function getColor() {
        return $this->color;
    }

    function getWeight() {
        return $this->weight;
    }

    function getSalePrice() {
        return $this->salePrice;
    }

    function getCostPrice() {
        return $this->costPrice;
    }

    function getCostCurrency() {
        return $this->costCurrency;
    }

    function getSaleCurrency() {
        return $this->saleCurrency;
    }

    function getImage() {
        return $this->image;
    }

    function getCreatedDate() {
        return $this->createdDate;
    }

    function getStock() {
        return $this->stock;
    }

    function getDescription() {
        return $this->description;
    }

    function getTags() {
        return $this->tags;
    }

    function setBrand($brand) {
        $this->brand = $brand;
    }

    function setModel($model) {
        $this->model = $model;
    }

    function setColor($color) {
        $this->color = $color;
    }

    function setWeight($weight) {
        $this->weight = $weight;
    }

    function setSalePrice($salePrice) {
        $this->salePrice = $salePrice;
    }

    function setCostPrice($costPrice) {
        $this->costPrice = $costPrice;
    }

    function setCostCurrency($costCurrency) {
        $this->costCurrency = $costCurrency;
    }

    function setSaleCurrency($saleCurrency) {
        $this->saleCurrency = $saleCurrency;
    }

    function setImage($image) {
        $this->image = $image;
    }

    function setCreatedDate($createdDate) {
        $this->createdDate = $createdDate;
    }

    function setStock($stock) {
        $this->stock = $stock;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setTags($tags) {
        $this->tags = $tags;
    }

}