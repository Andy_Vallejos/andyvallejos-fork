<?php
/* 
 * Copyright (C) PowerOn Sistemas - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Lucas Sosa <sosalucas87@gmail.com>, Diciembre 2020
 */
declare(strict_types=1);
namespace App\Domain\Repositories;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;

class MainRepository extends EntityRepository {
    
    public function paginate(QueryBuilder $queryBuilder, $page = 1, int $limit = 50) {
        $query = $queryBuilder->getQuery();
        
        $p = new Paginator($query);
        if (!$page || $page < 0 || !is_numeric($page)) {
            $page = 1;
        }
        $p->setUseOutputWalkers(false)->getQuery()
            ->setFirstResult($limit * ($page - 1))
            ->setMaxResults($limit)
        ;
        
        $results = $p->getQuery()->getResult(Query::HYDRATE_ARRAY);
        $allResults = $p->count();
        return [
            'maxPages' => ceil($allResults / $limit),
            'maxResults' => $limit,
            'currentPage' => $page,
            'countAllResults' => $allResults,
            'countPaginatedResults' => count($results),
            'results' => $results
        ];
    }
}