<?php
/* 
 * Copyright (C) PowerOn Sistemas - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Lucas Sosa <sosalucas87@gmail.com>, Diciembre 2020
 */
declare(strict_types=1);

namespace App\Application\Actions;

use App\Domain\DomainException\DomainRecordNotFoundException;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Log\LoggerInterface;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\ConstraintViolation;

abstract class Action {
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Response
     */
    protected $response;

    /**
     * @var array
     */
    protected $args;
    
    /**
     * @var EntityManager
     */
    protected $entityManager;


    /**
     * @param LoggerInterface $logger
     * @param EntityManager $entityManager
     */
    public function __construct(LoggerInterface $logger, EntityManager $entityManager) {
        $this->logger = $logger;
        $this->entityManager = $entityManager;
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param array    $args
     * @return Response
     * @throws HttpNotFoundException
     * @throws HttpBadRequestException
     */
    public function __invoke(Request $request, Response $response, $args): Response {
        $this->request = $request;
        $this->response = $response;
        $this->args = $args;
        try {
            return $this->action();
        } catch (DomainRecordNotFoundException $e) {
            throw new HttpNotFoundException($this->request, $e->getMessage());
        }
    }

    /**
     * @return Response
     * @throws DomainRecordNotFoundException
     * @throws HttpBadRequestException
     */
    abstract protected function action(): Response;


    /**
     * @return array|object
     * @throws HttpBadRequestException
     */
    protected function getFormData() {
        $input = json_decode(file_get_contents('php://input'));

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new HttpBadRequestException($this->request, 'La solicitud no es de tipo JSON.');
        }

        return $input;
    }

    /**
     * @param  string $name
     * @return mixed
     * @throws HttpBadRequestException
     */
    protected function resolveArg(string $name) {
        if ( !isset($this->args[$name]) ) {
            throw new HttpBadRequestException($this->request, "El parámetro `{$name}` no existe.");
        }

        return $this->args[$name];
    }

    protected function resolveQueryParam(string $name) {
        $params = $this->request->getQueryParams();
        
        return isset($params[$name]) ? $params[$name] : null;
    }
    
    /**
     * @param  array|object|null $data
     * @return Response
     */
    protected function respondWithMessage(?string $message = null, int $statusCode = 200): Response {
        $payload = new ActionPayload($message, null, $statusCode);

        return $this->respond($payload);
    }
    
    /**
     * @param  array|object|null $data
     * @return Response
     */
    protected function respondWithData(?string $message = null, $data = null, int $statusCode = 200): Response {
        $payload = new ActionPayload($message, $data, $statusCode);

        return $this->respond($payload);
    }

    /**
     * @param ActionPayload $payload
     * @return Response
     */
    protected function respond(ActionPayload $payload): Response {
        $json = json_encode($payload, JSON_PRETTY_PRINT | JSON_NUMERIC_CHECK);
        $this->response->getBody()->write($json);

        return $this->response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus($payload->getStatusCode())
        ;
    }
    /**
     * @param ConstraintViolation[] $errors
     * @return array
     */
    protected function parseErrors(array $errors): array {
        $parsedErrors = [];
        foreach ($errors as $error) {
            $parsedErrors[$error->getPropertyPath()] = $error->getMessage();
        }
        
        return $parsedErrors;
    }
}
