<?php
/* 
 * Copyright (C) PowerOn Sistemas - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Lucas Sosa <sosalucas87@gmail.com>, Diciembre 2020
 */
declare(strict_types=1);

namespace App\Application\Actions\Product;

use Psr\Http\Message\ResponseInterface as Response;

class ListProductsAction extends ProductAction {
    /**
     * {@inheritdoc}
     */
    protected function action(): Response {
        $queryBuilder = $this->repository->createQueryBuilder('p');
        $queryBuilder->select(
            'p.id', 'p.brand', 'p.model', 'p.createdDate', 'p.salePrice', 'p.saleCurrency', 
            'p.stock', 'p.color', 'p.weight', 'p.image', 'p.description', 'p.tags'
        );
        $paginator = $this->repository->paginate($queryBuilder, $this->resolveQueryParam('p'), 5);
        $hashids = $this->hashids;
        $paginator['results'] = array_map(function($product) use($hashids) {
            $product['id'] = $hashids->encode($product['id']);
            return $product;
        }, $paginator['results']);
                        
        return $this->respondWithData(null, $paginator);
    }
}
