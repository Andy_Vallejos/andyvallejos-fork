<?php
/* 
 * Copyright (C) PowerOn Sistemas - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Lucas Sosa <sosalucas87@gmail.com>, Diciembre 2020
 */
declare(strict_types=1);

namespace App\Application\Actions\Product;

use Psr\Http\Message\ResponseInterface as Response;
use App\Domain\Entities\Product;

class ProcessProductAction extends ProductAction {
    /**
     * {@inheritdoc}
     */
    protected function action(): Response {
        // $data = $this->getFormData();
        $productId = key_exists('productId', $this->args) ? $this->args['productId'] : null;

        $product = new Product();
        $product->setBrand('la');
        $product->setModel('Light');
        $product->setCreatedDate(new \DateTime());
        /* @var $errors \Symfony\Component\Validator\ConstraintViolation[]  */
        $errors = $this->validator->validate($product);
        if (count($errors) == 0) {
            return $this->respondWithData('Verifique los errores del formulario', $this->parseErrors($errors));
        }
        
        $this->entityManager->persist($product);
        $this->entityManager->flush();
        
        
        return $this->respondWithData(sprintf('El producto fue %s correctamente', $productId ? 'agregado' : 'modificado'), [
            
        ]);
    }
}
