<?php
/* 
 * Copyright (C) PowerOn Sistemas - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Lucas Sosa <sosalucas87@gmail.com>, Diciembre 2020
 */
declare(strict_types=1);

namespace App\Application\Actions;

use JsonSerializable;

class ActionPayload implements JsonSerializable {
    /**
     * @var int
     */
    private $statusCode;
    /**
     * @var string
     */
    private $message;

    /**
     * @var array
     */
    private $data;

    /**
     * @var ActionError|null
     */
    private $error;

    /**
     * @param string                $message
     * @param array|object|null     $data
     * @param int                   $statusCode
     * @param ActionError|null      $error
     */
    public function __construct(?string $message = null, $data = null, int $statusCode = 200, ?ActionError $error = null) {
        $this->statusCode = $statusCode;
        $this->data = $data;
        $this->error = $error;
        $this->message = $message;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int {
        return $this->statusCode;
    }

    /**
     * @return array
     */
    public function getData(): array {
        return $this->data;
    }

    /**
     * @return ActionError|null
     */
    public function getError(): ?ActionError {
        return $this->error;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array {
        $payload = [
            'statusCode' => $this->statusCode,
            'data' => $this->data,
            'message' => $this->message
        ];

        if ($this->error !== null) {
            $payload['error'] = $this->error;
        }

        return $payload;
    }
}
